import React from 'react';

const Form = () => {
  return (
    <form>
      <div>
        <label htmlFor="email">Email</label>
        <input type="text" name="email" id="email" />
      </div>

      <div>
        <label htmlFor="password">Password</label>
        <input type="password" name="password" id="password" />
      </div>
      <button type="submit">Log in</button>
    </form>
  );
};

export default Form;
